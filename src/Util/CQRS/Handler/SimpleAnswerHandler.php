<?php

namespace App\Util\CQRS\Handler;

use App\DTO\Flashcard\Command\SimpleAnswer;
use App\Entity\FlashcardAnswer;
use Doctrine\ORM\EntityManagerInterface;

class SimpleAnswerHandler
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(SimpleAnswer $command)
    {
        $answer = new FlashcardAnswer();
        $answer->flashcard = $command->flashcard;
        $answer->answer = $command->answer === true ? 'YES' : 'NO';
        $answer->type = 'SIMPLE';

        $this->entityManager->persist($answer);
        $this->entityManager->flush();
    }
}
