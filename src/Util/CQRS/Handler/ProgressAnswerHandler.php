<?php

namespace App\Util\CQRS\Handler;

use App\DTO\Flashcard\Command\ProgressAnswer;
use App\Entity\FlashcardAnswer;
use Doctrine\ORM\EntityManagerInterface;

class ProgressAnswerHandler
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(ProgressAnswer $command)
    {

        $answer = new FlashcardAnswer();
        $answer->flashcard = $command->flashcard;
        $answer->type = 'PROGRESS';
        switch ($command->answer) {
            case 0;
                $answer->answer = 'NO';
                break;
            case 1:
                $answer->answer = 'MAYBE';
                break;
            case 2:
                $answer->answer = 'YES';
                break;
        }

        $this->entityManager->persist($answer);
        $this->entityManager->flush();
    }
}
