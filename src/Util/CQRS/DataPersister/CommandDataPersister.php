<?php
namespace App\Util\CQRS\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Util\CQRS\Command;
use League\Tactician\CommandBus;

class CommandDataPersister implements DataPersisterInterface
{
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function supports($data): bool
    {
        return $data instanceof Command;
    }

    public function persist($data)
    {
        $this->commandBus->handle($data);
    }

    public function remove($data)
    {
    }
}
