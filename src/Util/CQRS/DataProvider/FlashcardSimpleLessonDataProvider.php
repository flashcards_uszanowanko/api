<?php

namespace App\Util\CQRS\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\DTO\Flashcard\Query\FlashcardSimpleLesson;
use App\Entity\Lesson;
use Doctrine\ORM\EntityManagerInterface;

class FlashcardSimpleLessonDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return FlashcardSimpleLesson::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $identifiers, string $operationName = null, array $context = [])
    {
        $lessonRepository = $this->entityManager->getRepository(Lesson::class);
        $lesson = $lessonRepository->find($identifiers);

        $result = new FlashcardSimpleLesson();
        $result->lesson = $lesson->name;
        $result->flashcards = $lesson->flashcards;

        return $result;
    }
}
