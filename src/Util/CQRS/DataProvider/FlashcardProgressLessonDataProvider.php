<?php

namespace App\Util\CQRS\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\DTO\Flashcard\Query\FlashcardProgressLesson;
use App\DTO\Flashcard\Query\FlashcardSimpleLesson;
use App\Entity\FlashcardAnswer;
use App\Entity\Lesson;
use Doctrine\ORM\EntityManagerInterface;

class FlashcardProgressLessonDataProvider
    implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    private $entityManager;

    private $lessonRepository;

    private $answerRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->lessonRepository = $this->entityManager->getRepository(Lesson::class);;
        $this->answerRepository = $this->entityManager->getRepository(FlashcardAnswer::class);;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return FlashcardProgressLesson::class === $resourceClass;
    }

    public function getItem(
        string $resourceClass,
        $identifiers,
        string $operationName = null,
        array $context = []
    ) {
        $lesson = $this->lessonRepository->find($identifiers);

        $result = new FlashcardProgressLesson();
        $result->lesson = $lesson->name;
        $result->flashcards = [];
        foreach ($lesson->flashcards as $flashcard) {
            $flashcardAnswer = $this->answerRepository->findBy(
                [
                    "flashcard" => $flashcard->getId(),
                    "type" => "PROGRESS",
                    "answer" => "YES",
                ]
            );
            if (!empty($flashcardAnswer)) {
                continue;
            }
            $result->flashcards[] = $flashcard;
        }

        return $result;
    }
}
