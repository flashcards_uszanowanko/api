<?php

namespace App\Util\Doctrine;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Flashcard;
use App\Entity\Lesson;
use App\Entity\Subject;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OwnerExtension implements
    QueryCollectionExtensionInterface,
    QueryItemExtensionInterface

{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ) {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass)
    {
        if ($this->tokenStorage->getToken() === null) {
            return;
        }
        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user instanceof User) {
            return;
        }
        $rootAlias = $queryBuilder->getRootAliases()[0];

        switch ($resourceClass) {
            case Subject::class:
                $queryBuilder->andWhere(sprintf('%s.user = :current_user', $rootAlias));
                break;
            case Lesson::class:
                $queryBuilder->join(sprintf('%s.subject', $rootAlias), 'owner_subject');
                $queryBuilder->andWhere('owner_subject.user = :current_user');
                break;
            case Flashcard::class:
                $queryBuilder->join(sprintf('%s.lesson', $rootAlias), 'owner_lesson');
                $queryBuilder->join('owner_lesson.subject', 'owner_subject');
                $queryBuilder->andWhere('owner_subject.user = :current_user');
                break;
            default:
                return;
        }

        $queryBuilder->setParameter('current_user', $user->getId());
    }
}
