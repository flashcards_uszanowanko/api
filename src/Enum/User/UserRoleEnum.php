<?php

namespace App\Enum\User;

class UserRoleEnum
{
    const ADMIN = 'ROLE_ADMIN';
    const USER = 'ROLE_USER';

    /**
     * @return array
     */
    public static function getValues()
    {
        return [
            self::ADMIN,
            self::USER,
        ];
    }
}
