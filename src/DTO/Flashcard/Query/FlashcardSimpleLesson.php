<?php

namespace App\DTO\Flashcard\Query;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Flashcard;
use App\Util\CQRS\Query;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"flashcardDetails","simpleDetails"}},
 *     collectionOperations={},
 *     itemOperations={
 *"        get"={
 *             "method"="GET",
 *             "path"="/lesson/{id}/simple/"
 *         }
 *     }
 *  )
 * )
 */
class FlashcardSimpleLesson implements Query
{
    /**
     * @ApiProperty(identifier=true)
     * @var string
     * @Groups({"simpleDetails"})
     */
    public $lesson;

    /**
     * @var Flashcard[]
     * @Groups({"simpleDetails"})
     */
    public $flashcards;
}
