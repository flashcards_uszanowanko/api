<?php

namespace App\DTO\Flashcard\Query;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Flashcard;
use App\Util\CQRS\Query;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"flashcardDetails","progressDetails"}},
 *     collectionOperations={},
 *     itemOperations={
 *"        get"={
 *             "method"="GET",
 *             "path"="/lesson/{id}/progress/"
 *         }
 *     }
 *  )
 * )
 */
class FlashcardProgressLesson implements Query
{
    /**
     * @ApiProperty(identifier=true)
     * @var string
     * @Groups({"progressDetails"})
     */
    public $lesson;

    /**
     * @var Flashcard[]
     * @Groups({"progressDetails"})
     */
    public $flashcards;
}
