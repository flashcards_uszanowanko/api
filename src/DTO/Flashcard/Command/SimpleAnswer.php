<?php

namespace App\DTO\Flashcard\Command;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Flashcard;
use App\Util\CQRS\Command;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "path"="/answer/simple"
 *          }
 *      },
 *      itemOperations={}
 *  )
 * )
 */
class SimpleAnswer implements Command
{
    /**
     * @var boolean
     * @Assert\NotBlank()
     */
    public $answer;

    /**
     * @var Flashcard
     * @Assert\NotBlank()
     */
    public $flashcard;
}
