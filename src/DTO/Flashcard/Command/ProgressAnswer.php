<?php

namespace App\DTO\Flashcard\Command;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Flashcard;
use App\Util\CQRS\Command;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "path"="/answer/progress"
 *          }
 *      },
 *      itemOperations={}
 *  )
 * )
 */
class ProgressAnswer implements Command
{
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Range(min=0, max=2)
     */
    public $answer;

    /**
     * @var Flashcard
     * @Assert\NotBlank()
     */
    public $flashcard;
}
