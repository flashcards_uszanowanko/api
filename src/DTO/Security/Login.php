<?php

namespace App\DTO\Security;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "path"="/login",
 *              "swagger_context"={
 *                  "summary" = "Login into system",
 *                  "parameters" = {
 *                      {
 *                          "name"="login",
 *                          "in"="body",
 *                          "description"="Login into system",
 *                          "schema"={
 *                              "type" = "object",
 *                              "required" = {
 *                                  "email",
 *                                  "password",
 *                              },
 *                              "properties" = {
 *                                   "email" = {
 *                                      "type" = "string"
 *                                   },
 *                                   "password" = {
 *                                      "type" = "string"
 *                                   },
 *                              }
 *                          }
 *                      }
 *                  },
 *                  "responses"={
 *                     200={
 *                          "description"="User login",
 *                          "schema"={
 *                              "type" = "object",
 *                              "properties" = {
 *                                   "token" = {
 *                                      "type" = "string"
 *                                   },
 *                              }
 *                          }
 *                     },
 *                     401={
 *                          "description"="Bad credentials",
 *                          "schema"={
 *                              "type" = "object",
 *                              "properties" = {
 *                                   "code" = {
 *                                      "type" = "string"
 *                                   },
 *                                    "message" = {
 *                                      "type" = "string"
 *                                   },
 *                              }
 *                          }
 *                     }
 *                  }
 *              }
 *          }
 *      },
 *      itemOperations={}
 *  )
 */
class Login
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @param string $email
     * @param string $password
     */
    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
