<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      attributes={
 *           "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *      },
 *      normalizationContext={"groups"={"subjectDetails"}},
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"subjectList"}},
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"subjectCreate"}},
 *              "validation_groups"={"subjectCreate"}
 *          }
 *      },
 *      itemOperations={
 *          "get" = {
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object.user.getId() == user.getId()"
 *          },
 *          "put"={
 *               "denormalization_context"={"groups"={"subjectUpdate"}},
 *               "validation_groups"={"subjectUpdate"},
 *               "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object.user.getId() == user.getId()"
 *          },
 *          "delete"={
 *               "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object.user.getId() == user.getId()"
 *          }
 *      }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "name": "partial"
 *     }
 * )
 * @ORM\Entity
 */
class Subject
{
    /**
     * @var int The entity Id
     *
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Util\Doctrine\UuidIdGenerator")
     * @Groups({"subjectList", "subjectDetails"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"subjectCreate","subjectUpdate"})
     * @Assert\Length(max="255", groups={"subjectCreate","subjectUpdate"})
     * @Groups({"subjectList", "subjectDetails", "subjectCreate", "subjectUpdate"})
     */
    public $name;

    /**
     * @ORM\OneToMany(targetEntity="Lesson", mappedBy="subject", cascade={"persist", "remove"})
     * @ApiSubresource()
     */
    public $lessons;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Assert\NotBlank(groups={"subjectCreate"})
     */
    public $user;

    /**
     * @param string $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }
}

