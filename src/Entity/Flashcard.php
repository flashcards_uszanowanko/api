<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      attributes={
 *           "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *      },
 *      normalizationContext={"groups"={"flashcardDetails"}},
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"flashcardList"}}
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"flashcardCreate"}},
 *              "validation_groups"={"flashcardCreate"}
 *          },
 *          "api_lessons_flashcards_get_subresource" = {
 *              "normalization_context"={"groups"={"flashcardList"}},
 *          }
 *      },
 *      itemOperations={
 *          "get",
 *          "put"={
 *               "denormalization_context"={"groups"={"flashcardUpdate"}},
 *               "validation_groups"={"flashcardUpdate"}
 *          },
 *          "delete"
 *      }
 * )
 *
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "id": "exact",
 *          "question": "partial",
 *          "answer": "partial"
 *     }
 * )
 *
 * @ORM\Entity
 */
class Flashcard
{
    /**
     * @var int The entity Id
     *
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Util\Doctrine\UuidIdGenerator")
     * @Groups({"flashcardList", "flashcardDetails"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"flashcardCreate","flashcardCreate"})
     * @Assert\Length(max="255", groups={"flashcardCreate","flashcardCreate"})
     * @Groups({"flashcardList", "flashcardDetails", "flashcardCreate", "flashcardUpdate"})
     */
    public $question;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"flashcardCreate","flashcardCreate"})
     * @Assert\Length(max="255", groups={"flashcardCreate","flashcardCreate"})
     * @Groups({"flashcardList", "flashcardDetails", "flashcardCreate", "flashcardUpdate"})
     */
    public $answer;

    /**
     * @Assert\NotBlank(groups={"flashcardCreate"})
     * @ORM\ManyToOne(targetEntity="Lesson", inversedBy="flashcards")
     * @Groups({"flashcardCreate"})
     */
    public $lesson;

    /**
     * @param string $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }
}
