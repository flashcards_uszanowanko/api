<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      attributes={
 *           "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *      },
 *      normalizationContext={"groups"={"lessonDetails"}},
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"lessonList"}},
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"lessonCreate"}},
 *              "validation_groups"={"lessonCreate"}
 *          },
 *         "api_subjects_lessons_get_subresource" = {
 *              "normalization_context"={"groups"={"lessonList"}},
 *          }
 *      },
 *      itemOperations={
 *          "get" ={
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object.subject.user.getId() == user.getId()"
 *          },
 *          "put"={
 *              "denormalization_context"={"groups"={"lessonUpdate"}},
 *              "validation_groups"={"lessonUpdate"},
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object.subject.user.getId() == user.getId()"
 *          },
 *          "delete"={
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object.subject.user.getId() == user.getId()"
 *          }
 *      }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "name": "partial"
 *     }
 * )
 * @ORM\Entity
 */
class Lesson
{
    /**
     * @var int The entity Id
     *
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Util\Doctrine\UuidIdGenerator")
     * @Groups({"lessonList", "lessonDetails"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"lessonCreate","lessonCreate"})
     * @Assert\Length(max="255", groups={"lessonCreate","lessonCreate"})
     * @Groups({"lessonList", "lessonDetails", "lessonCreate", "lessonUpdate"})
     */
    public $name;

    /**
     * @ORM\OneToMany(targetEntity="Flashcard", mappedBy="lesson", cascade={"persist", "remove"})
     * @ApiSubresource()
     */
    public $flashcards;

    /**
     * @ORM\ManyToOne(targetEntity="Subject", inversedBy="lessons")
     * @Assert\NotBlank(groups={"lessonCreate"})
     * @Groups({"lessonCreate"})
     */
    public $subject;

    /**
     * @param string $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }
}

