<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ApiResource(
 *      collectionOperations={
 *          "get"
 *      },
 *      itemOperations={
 *          "get"
 *      }
 * )
 */
class FlashcardAnswer
{
    /**
     * @var string The entity Id
     * @ORM\Column(name="id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Util\Doctrine\UuidIdGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    public $answer;

    /**
     * @ORM\Column(type="string", length=10)
     */
    public $type;

    /**
     * @ORM\ManyToOne(targetEntity="Flashcard")
     */
    public $flashcard;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
