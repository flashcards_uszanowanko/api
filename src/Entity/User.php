<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      attributes={
 *           "access_control"=" is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') and object.getId() == user.getId() )",
 *           "access_control_message"="Only admins or owner have access to user data."
 *      },
 *      normalizationContext={"groups"={"userDetails"}},
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"userList"}},
 *              "access_control"="is_granted('ROLE_ADMIN')",
 *              "access_control_message"="Only admins can get user list."
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"userCreate"}},
 *              "validation_groups"={"userCreate"},
 *              "access_control"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          }
 *      },
 *      itemOperations={
 *          "get",
 *          "put"={
 *               "denormalization_context"={"groups"={"userUpdate"}},
 *               "validation_groups"={"userUpdate"}
 *          },
 *          "changePassword"={
 *              "method"="PUT",
 *              "path"="/users/{id}/change-password",
 *              "denormalization_context"={"groups"={"userChangePassword"}},
 *              "validation_groups"={"userChangePassword"},
 *              "swagger_context"={
 *                  "summary" = "Change user password"
 *              }
 *          }
 *      }
 *  )
 *
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "id": "exact",
 *          "name": "partial",
 *          "email": "partial"
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 * @UniqueEntity("email")
 */
class User implements UserInterface
{
    /**
     * @var string The entity Id
     *
     * @ORM\Column(type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Util\Doctrine\UuidIdGenerator")
     *
     * @Groups({"userList", "userDetails"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"userCreate","userUpdate"})
     * @Assert\Length(max="255", groups={"userCreate","userUpdate"})
     *
     * @Groups({"userList", "userDetails", "userCreate", "userUpdate"})
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email(groups={"userCreate"})
     * @Assert\NotBlank(groups={"userCreate"})
     * @Assert\Length(max="255", groups={"userCreate"})
     *
     * @Groups({"userList", "userDetails", "userCreate"})
     */
    public $email;

    /**
     * @ORM\Column(type="string", length=64)
     */
    public $password;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     * @Assert\NotBlank(groups={"userCreate"})
     * @Assert\Choice(callback={"App\Enum\User\UserRoleEnum", "getValues"}, multiple=true, groups={"userCreate"})
     * @Groups({"userDetails", "userCreate"})
     *
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={
     *             "type"="array",
     *             "enum"={"ROLE_ADMIN", "ROLE_USER"},
     *             "example"={"ROLE_USER"}
     *         }
     *     })
     */
    public $roles;

    /**
     * @Assert\NotBlank(groups={"userCreate", "userChangePassword"})
     * @Assert\Length(min="8", max="255", groups={"userCreate", "userChangePassword"})
     *
     * @Groups({"userCreate", "userChangePassword"})
     *
     * @var string
     */
    public $plainPassword;

    /**
     * @param string $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string[] The user roles
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string The username
     */
    public function getUsername()
    {
        return $this->name;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
}
