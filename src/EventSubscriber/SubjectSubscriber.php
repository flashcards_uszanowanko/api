<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Subject;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SubjectSubscriber implements EventSubscriberInterface
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                'setupSubjectOwner',
                EventPriorities::PRE_VALIDATE,
            ],
        ];
    }

    public function setupSubjectOwner(GetResponseForControllerResultEvent $event)
    {
        $subject = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$subject instanceof Subject ||
            Request::METHOD_POST !== $method ||
            $this->tokenStorage->getToken() === null) {
            return;
        }
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            $subject->user = $user;
        }
    }
}
