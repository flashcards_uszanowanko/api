<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\DTO\Flashcard\SimpleAnswer;
use App\Entity\Subject;
use App\Entity\User;
use App\Util\CQRS\Command;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CommandSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['command', EventPriorities::PRE_SERIALIZE,],
        ];
    }

    public function command(
        GetResponseForControllerResultEvent $event
    ) {
        $subject = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$subject instanceof Command || Request::METHOD_POST !== $method) {
            return;
        }

        $event->setResponse(new Response('', 201));
    }
}
