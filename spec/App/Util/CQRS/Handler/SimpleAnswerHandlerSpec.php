<?php

namespace spec\App\Util\CQRS\Handler;

use App\DTO\Flashcard\Command\SimpleAnswer;
use App\Entity\Flashcard;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SimpleAnswerHandlerSpec extends ObjectBehavior
{
    function let(EntityManagerInterface $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }

    function it_should_store_negative_answer_in_database(EntityManagerInterface $entityManager)
    {
        $answer = new SimpleAnswer();
        $answer->flashcard = new Flashcard();
        $answer->answer = false;

        $entityManager->persist(
            Argument::that(
                function ($object) {
                    if ($object->answer === 'NO' && $object->type === 'SIMPLE') {
                        return true;
                    }

                    return false;
                }
            )
        )->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->handle($answer);
    }

    function it_should_store_positive_answer_in_database(EntityManagerInterface $entityManager)
    {
        $answer = new SimpleAnswer();
        $answer->flashcard = new Flashcard();
        $answer->answer = true;

        $entityManager->persist(
            Argument::that(
                function ($object) {
                    if ($object->answer === 'YES' && $object->type === 'SIMPLE') {
                        return true;
                    }

                    return false;
                }
            )
        )->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->handle($answer);
    }
}
