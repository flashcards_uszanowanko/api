<?php

namespace spec\App\Util\CQRS\Handler;

use App\DTO\Flashcard\Command\ProgressAnswer;
use App\Entity\Flashcard;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ProgressAnswerHandlerSpec extends ObjectBehavior
{
    function let(EntityManagerInterface $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }

    function it_should_store_negative_answer_in_database(EntityManagerInterface $entityManager)
    {
        $answer = new ProgressAnswer();
        $answer->flashcard = new Flashcard();
        $answer->answer = 0;
        $entityManager->persist(
            Argument::that(
                function ($object) {
                    if ($object->answer === 'NO' && $object->type === 'PROGRESS') {
                        return true;
                    }

                    return false;
                }
            )
        )->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->handle($answer);
    }

    function it_should_store_positive_answer_in_database(EntityManagerInterface $entityManager)
    {
        $answer = new ProgressAnswer();
        $answer->flashcard = new Flashcard();
        $answer->answer = 2;
        $entityManager->persist(
            Argument::that(
                function ($object) {
                    if ($object->answer === 'YES' && $object->type === 'PROGRESS') {
                        return true;
                    }

                    return false;
                }
            )
        )->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->handle($answer);
    }

    function it_should_store_maybe_answer_in_database(EntityManagerInterface $entityManager)
    {
        $answer = new ProgressAnswer();
        $answer->flashcard = new Flashcard();
        $answer->answer = 1;
        $entityManager->persist(
            Argument::that(
                function ($object) {
                    if ($object->answer === 'MAYBE' && $object->type === 'PROGRESS') {
                        return true;
                    }

                    return false;
                }
            )
        )->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->handle($answer);
    }
}
