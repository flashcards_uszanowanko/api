<?php

namespace spec\App\Util\CQRS\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\DTO\Flashcard\Query\FlashcardSimpleLesson;
use App\Entity\Flashcard;
use App\Entity\Lesson;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PhpSpec\ObjectBehavior;

class FlashcardSimpleLessonDataProviderSpec extends ObjectBehavior
{
    function let(EntityManagerInterface $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }

    function it_should_implement_item_data_provider_interface()
    {
        $this->shouldImplement(ItemDataProviderInterface::class);
    }

    function it_should_implement_restricted_data_provider_interface()
    {
        $this->shouldImplement(RestrictedDataProviderInterface::class);
    }

    function it_should_support_flashcard_simple_lesson(){
        $this->supports(FlashcardSimpleLesson::class)->shouldBe(true);
        $this->supports("somethingElse")->shouldBe(false);
    }

    function it_should_search_flashcard_for_given_lesson_and_prepare_response(
        EntityManagerInterface $entityManager,
        EntityRepository $lessonRepository
    ) {
        $lesson = new Lesson('lessonUUID');
        $lesson->name = "DummyName";
        $lesson->flashcards = [
            new Flashcard(),
            new Flashcard()
        ];
        $lessonRepository->find('lessonUUID')->willReturn($lesson);
        $entityManager->getRepository(Lesson::class)->willReturn($lessonRepository);


        $result = $this->getItem(FlashcardSimpleLesson::class,'lessonUUID');
        $result->lesson->shouldBe("DummyName");
        $result->flashcards->shouldHaveCount(2);
    }
}
