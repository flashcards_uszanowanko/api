<?php

namespace spec\App\Util\CQRS\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Lesson;
use App\Util\CQRS\Command;
use League\Tactician\CommandBus;
use PhpSpec\ObjectBehavior;

class CommandDataPersisterSpec extends ObjectBehavior
{
    function let(CommandBus $commandBus)
    {
        $this->beConstructedWith($commandBus);
    }

    function it_should_implement_query_item_extension()
    {
        $this->shouldImplement(DataPersisterInterface::class);
    }

    function it_should_support_command_objects(Command $command, Lesson $lesson)
    {
        $this->supports($command)->shouldBe(true);
        $this->supports($lesson)->shouldBe(false);
    }

    function it_should_pass_command_into_command_bus(Command $command, CommandBus $commandBus)
    {
        $commandBus->handle($command)->shouldBeCalled();

        $this->persist($command);
    }
}
