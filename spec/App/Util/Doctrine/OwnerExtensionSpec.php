<?php

namespace spec\App\Util\Doctrine;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Flashcard;
use App\Entity\Lesson;
use App\Entity\Subject;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class OwnerExtensionSpec extends ObjectBehavior
{
    function let(TokenStorageInterface $tokenStorage, TokenInterface $token)
    {
        $user = new User('userUUID');
        $token->getUser()->willReturn($user);
        $tokenStorage->getToken()->willReturn($token);
        $this->beConstructedWith($tokenStorage);
    }

    function it_should_implement_query_item_extension()
    {
        $this->shouldImplement(QueryItemExtensionInterface::class);
    }

    function it_should_implement_query_collection_extension()
    {
        $this->shouldImplement(QueryCollectionExtensionInterface::class);
    }

    function it_should_add_filer_param_to_query_if_user_is_logedin_and_we_tray_to_get_list_of_subjects(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator
    ) {
        $queryBuilder->getRootAliases()->willreturn(['o']);
        $queryBuilder->andWhere(Argument::any())->shouldBeCalled();
        $queryBuilder->setParameter('current_user', 'userUUID')->shouldBeCalled();

        $this->applyToCollection($queryBuilder, $queryNameGenerator, Subject::class);
    }

    function it_should_add_filer_param_to_query_if_user_is_logedin_and_we_tray_to_get_single_subject(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator
    ) {
        $queryBuilder->getRootAliases()->willreturn(['o']);
        $queryBuilder->andWhere(Argument::any())->shouldBeCalled();
        $queryBuilder->setParameter('current_user', 'userUUID')->shouldBeCalled();

        $this->applyToItem($queryBuilder, $queryNameGenerator, Subject::class, ["dumyId"]);
    }

    function it_should_add_filer_param_to_query_if_user_is_logedin_and_we_tray_to_get_list_of_lessons(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator
    ) {
        $queryBuilder->getRootAliases()->willreturn(['o']);
        $queryBuilder->andWhere(Argument::any())->shouldBeCalled();
        $queryBuilder->join(Argument::type('string'), Argument::type('string'))->shouldBeCalled();
        $queryBuilder->setParameter('current_user', 'userUUID')->shouldBeCalled();

        $this->applyToCollection($queryBuilder, $queryNameGenerator, Lesson::class);
    }

    function it_should_add_filer_param_to_query_if_user_is_logedin_and_we_tray_to_get_single_lessons(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator
    ) {
        $queryBuilder->getRootAliases()->willreturn(['o']);
        $queryBuilder->andWhere(Argument::any())->shouldBeCalled();
        $queryBuilder->join(Argument::type('string'), Argument::type('string'))->shouldBeCalled();
        $queryBuilder->setParameter('current_user', 'userUUID')->shouldBeCalled();

        $this->applyToItem($queryBuilder, $queryNameGenerator, Lesson::class, ["dumyId"]);
    }

    function it_should_add_filer_param_to_query_if_user_is_logedin_and_we_tray_to_get_list_of_flashcard(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator
    ) {
        $queryBuilder->getRootAliases()->willreturn(['o']);
        $queryBuilder->andWhere(Argument::any())->shouldBeCalled();
        $queryBuilder->join(Argument::type('string'), Argument::type('string'))->shouldBeCalledTimes(2);
        $queryBuilder->setParameter('current_user', 'userUUID')->shouldBeCalled();

        $this->applyToCollection($queryBuilder, $queryNameGenerator, Flashcard::class);
    }

    function it_should_add_filer_param_to_query_if_user_is_logedin_and_we_tray_to_get_single_flashcard(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator
    ) {
        $queryBuilder->getRootAliases()->willreturn(['o']);
        $queryBuilder->andWhere(Argument::any())->shouldBeCalled();
        $queryBuilder->join(Argument::type('string'), Argument::type('string'))->shouldBeCalledTimes(2);
        $queryBuilder->setParameter('current_user', 'userUUID')->shouldBeCalled();

        $this->applyToItem($queryBuilder, $queryNameGenerator, Flashcard::class, ["dumyId"]);
    }
}
