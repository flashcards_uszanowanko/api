<?php

namespace spec\App\EventSubscriber;

use App\Util\CQRS\Command;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class CommandSubscriberSpec extends ObjectBehavior
{
    function it_should_implement_event_subscriber_interface()
    {
        $this->shouldImplement(EventSubscriberInterface::class);
    }

    function it_should_be_executed_before_serialization()
    {
        $this->getSubscribedEvents()->shouldHaveKeyWithValue('kernel.view', ['command', 17]);
    }

    function it_should_brake_api_platform_object_life_cycle_if_object_is_command_and_method_was_post(
        GetResponseForControllerResultEvent $event,
        Request $request,
        Command $command
    ) {
        $request->getMethod()->willReturn('POST');

        $event->setResponse(Argument::type(Response::class))->shouldBeCalled();

        $event->getRequest()->willReturn($request);
        $event->getControllerResult()->willReturn($command);

        $this->command($event);
    }
}
