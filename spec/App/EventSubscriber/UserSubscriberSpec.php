<?php

namespace spec\App\EventSubscriber;

use App\Entity\Subject;
use App\Entity\User;
use PhpSpec\Exception\Example\FailureException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserSubscriberSpec extends ObjectBehavior
{
    function let(UserPasswordEncoderInterface $encoder)
    {
        $this->beConstructedWith($encoder);
    }

    function it_should_implement_event_subscriber_interface()
    {
        $this->shouldImplement(EventSubscriberInterface::class);
    }

    function it_should_be_executed_after_validation()
    {
        $this->getSubscribedEvents()->shouldHaveKeyWithValue('kernel.view', ['encodeUserPassword', 63]);
    }

    function it_should_encode_password_if_plain_password_was_set_and_method_is_post(
        UserPasswordEncoderInterface $encoder,
        GetResponseForControllerResultEvent $event,
        User $user,
        Request $request
    ) {

        $encoder->encodePassword($user, 'plainPassword')->willReturn('encodedPassword');

        $user->plainPassword = 'plainPassword';
        $user->eraseCredentials()->shouldBeCalled();

        $request->getMethod()->willReturn('POST');

        $event->getControllerResult()->willReturn($user);
        $event->getRequest()->willReturn($request);

        $this->encodeUserPassword($event);

        if ($user->password !== 'encodedPassword') {
            throw  new FailureException('Password should be equal to "encodedPassword"');
        }
    }

    function it_should_encode_password_if_plain_password_was_set_and_method_is_PUT(
        UserPasswordEncoderInterface $encoder,
        GetResponseForControllerResultEvent $event,
        User $user,
        Request $request
    ) {

        $encoder->encodePassword($user, 'plainPassword')->willReturn('encodedPassword');

        $user->plainPassword = 'plainPassword';
        $user->eraseCredentials()->shouldBeCalled();

        $request->getMethod()->willReturn('PUT');

        $event->getControllerResult()->willReturn($user);
        $event->getRequest()->willReturn($request);

        $this->encodeUserPassword($event);

        if ($user->password !== 'encodedPassword') {
            throw  new FailureException('Password should be equal to "encodedPassword"');
        }
    }

    function it_should_work_only_if_we_work_with_user(
        UserPasswordEncoderInterface $encoder,
        GetResponseForControllerResultEvent $event,
        Subject $subject,
        Request $request
    ) {
        $encoder->encodePassword(Argument::any(), Argument::any())->shouldNotBeCalled();

        $event->getControllerResult()->willReturn($subject);
        $event->getRequest()->willReturn($request);

        $this->encodeUserPassword($event);
    }
}
