<?php

namespace spec\App\EventSubscriber;

use App\Entity\Lesson;
use App\Entity\Subject;
use App\Entity\User;
use PhpSpec\Exception\Example\FailureException;
use PhpSpec\ObjectBehavior;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class SubjectSubscriberSpec extends ObjectBehavior
{
    function let(TokenStorageInterface $tokenStorage)
    {
        $this->beConstructedWith($tokenStorage);
    }

    function it_should_implement_event_subscriber_interface()
    {
        $this->shouldImplement(EventSubscriberInterface::class);
    }

    function it_should_be_executed_before_validation()
    {
        $this->getSubscribedEvents()->shouldHaveKeyWithValue('kernel.view', ['setupSubjectOwner', 65]);
    }

    function it_should_setup_user_for_subject_if_user_is_logged_in(
        TokenStorageInterface $tokenStorage,
        TokenInterface $token,
        GetResponseForControllerResultEvent $event,
        Request $request
    ) {
        $user = new User();
        $subject = new Subject();

        $tokenStorage->getToken()->willReturn($token);
        $token->getUser()->willReturn($user);

        $request->getMethod()->willReturn('POST');

        $event->getRequest()->willReturn($request);
        $event->getControllerResult()->willReturn($subject);

        $this->setupSubjectOwner($event);

        if ($subject->user !== $user) {
            throw  new FailureException('Users object should be equal');
        }
    }

    function it_should_skip_setuping_user_if_user_is_not_logged_in(
        TokenStorageInterface $tokenStorage,
        TokenInterface $token,
        GetResponseForControllerResultEvent $event,
        Request $request
    ) {
        $subject = new Subject();

        $tokenStorage->getToken()->willReturn(null);

        $request->getMethod()->willReturn('POST');

        $event->getRequest()->willReturn($request);
        $event->getControllerResult()->willReturn($subject);

        $this->setupSubjectOwner($event);

        if ($subject->user !== null) {
            throw  new FailureException('Users object should not be set');
        }
    }

    function it_should_skip_setuping_user_for_subject_if_request_method_is_not_post(
        TokenStorageInterface $tokenStorage,
        TokenInterface $token,
        GetResponseForControllerResultEvent $event,
        Request $request
    ) {
        $user = new User();
        $subject = new Subject();
        $subject->user = $user;

        $loggedUser = new User();

        $tokenStorage->getToken()->willReturn($token);
        $token->getUser()->willReturn($loggedUser);

        $request->getMethod()->willReturn('PUT');

        $event->getRequest()->willReturn($request);
        $event->getControllerResult()->willReturn($subject);

        $this->setupSubjectOwner($event);

        if ($subject->user !== $user) {
            throw  new FailureException('Users object not be changeg');
        }
    }

    function it_should_skip_setuping_user_if_event_is_not_about_subject(
        TokenStorageInterface $tokenStorage,
        TokenInterface $token,
        GetResponseForControllerResultEvent $event,
        Request $request
    ) {
        $lesson = new Lesson();

        $tokenStorage->getToken()->willReturn($token);
        $token->getUser()->shouldNotBeCalled();

        $request->getMethod()->willReturn('POST');

        $event->getRequest()->willReturn($request);
        $event->getControllerResult()->willReturn($lesson);

        $this->setupSubjectOwner($event);
    }
}
