<?php

use App\Entity\User;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Mink\Driver\BrowserKitDriver;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Coduo\PHPMatcher\Factory\SimpleFactory;
use Doctrine\Common\Persistence\ManagerRegistry;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;

/**
 * Defines application features from the specific contexbit.
 */
class RestContext extends RawMinkContext
{
    use KernelDictionary;

    private $userRepository;

    private $JWTManager;

    public function __construct(ManagerRegistry $doctrine, JWTManager $JWTManager)
    {
        $this->userRepository = $doctrine->getManager()->getRepository(User::class);
        $this->JWTManager = $JWTManager;
    }

    private $defaultHeaders = [
        'HTTP_ACCEPT' => 'application/ld+json',
        'CONTENT_TYPE' => 'application/ld+json',
    ];

    /**
     * @When I send a :method request to :path
     */
    public function iSendAGetRequestTo($method, $path)
    {
        $this->sendRequest($method, $path);
    }

    /**
     * @When user :user send a :method request to :path
     * @When user :user send a :method request to :path with body:
     */
    public function asSendARequestToWithBody($user, $method, $path, PyStringNode $body = null)
    {
        $user = $this->userRepository->findOneBy(['email' => $user]);
        if(!$user instanceof User){
            throw new Exception('User '.$user.' not found');
        }
        if($body instanceof PyStringNode){
            $body = $body->getRaw();
        }else{
            $body = '{}';
        }
        $token = $this->JWTManager->create($user);

        $this->sendRequest($method, $path, $body, array_merge(['HTTP_AUTHORIZATION' => 'Bearer ' . $token], $this->defaultHeaders));
    }

    /**
     * @Given I send a :method request to :path with body:
     */
    public function iSendARequestToWithBody($method, $path, PyStringNode $body)
    {
        $this->sendRequest($method, $path, $body->getRaw());
    }



    /**
     * @Then the response should contain json:
     */
    public function theResponseShouldContainJson(PyStringNode $jsonString)
    {
        $factory = new SimpleFactory();
        $matcher = $factory->createMatcher();

        if (!$matcher->match($this->getSession()->getPage()->getContent(), $jsonString->getRaw())) {
            throw new \RuntimeException($matcher->getError());
        }
    }

    /**
     * @param $method
     * @param $path
     * @param $body
     * @param $headers
     */
    private function sendRequest($method, $path, $body = '{}', $headers = [])
    {
        if (empty($headers)) {
            $headers = $this->defaultHeaders;
        }
        $driver = $this->getSession()->getDriver();
        if ($driver instanceof BrowserKitDriver) {
            $params = json_decode($body, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \RuntimeException(json_last_error_msg());
            }
            $driver->getClient()->request($method, $path, $params, [], $headers, $body);
        } else {
            throw new \RuntimeException('Unsupported driver. BrowserKit driver is required.');
        }
    }


}
