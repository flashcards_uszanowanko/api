<?php

use AppBundle\Entity\User;
use AppBundle\Util\Security\AclEstablish;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\BeforeFeatureScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\Yaml\Yaml;

/**
 * Defines application features from the specific context.
 */
class DatabaseContext implements Context
{

    private $doctrine;

    /**
     * @var array|null
     */
    private static $resetDatabaseSql;

    /**
     * @var array|null
     */
    private static $cachedObject;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $manager;

    /**
     * @var SchemaTool
     */
    private $schemaTool;

    /**
     * @var array
     */
    private $classes;

    /**
     * Initializes context.
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {

        $this->doctrine = $doctrine;
        $this->manager = $doctrine->getManager();
        $this->schemaTool = new SchemaTool($this->manager);
        $this->classes = $this->manager->getMetadataFactory()->getAllMetadata();
    }

    /** @BeforeScenario */
    public function BeforeScenario()
    {
        $this->createDatabase();
    }


    /**
     * @BeforeScenario @resetDatabaseCache
     */
    public function resetDatabase()
    {
        self::$resetDatabaseSql = null;
        self::$cachedObject = null;
        $this->BeforeScenario();
    }

    /**
     * @Given /^clear cache$/
     */
    public function clearCache()
    {
        $this->manager->clear();
    }

    private function createDatabase()
    {
        if (self::$resetDatabaseSql == null) {
            $sql = $this->schemaTool->getDropSchemaSQL($this->classes);
            $this->executeQuery($sql);

            $sqlCreate = $this->schemaTool->getCreateSchemaSql($this->classes);
            $this->executeQuery($sqlCreate);

            self::$resetDatabaseSql = array_merge($this->schemaTool->getDropSchemaSQL($this->classes), $sqlCreate);
        } else {
            $this->executeQuery(self::$resetDatabaseSql);
        }
    }


    private function executeQuery(array $sql)
    {
        $conn = $this->manager->getConnection();
        foreach ($sql as $query) {
            try {
                $conn->executeQuery($query);
            } catch (Exception $ex) {
            }
        }
    }
}
