@alice(User) @alice(Subject) @alice(Lesson) @alice(Flashcard)
Feature: Mange flashcards

  Scenario: User should be able to get list of flashcards
    When user "user1@test.dev" send a "GET" request to "/api/flashcards"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Flashcard",
      "@id": "\/api\/flashcards",
      "@type": "hydra:Collection",
      "hydra:member": [
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "piwo",
          "answer": "beer"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "beer",
          "answer": "piwo"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "direction",
          "answer": "kierunek"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "house",
          "answer": "dom"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "kierunek",
          "answer": "direction"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "dom",
          "answer": "house"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "pijany",
          "answer": "drunk"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "drunk",
          "answer": "pijany"
        }
      ],
      "hydra:totalItems": 8,
      "hydra:search": {
        "@type": "hydra:IriTemplate",
        "hydra:template": "\/api\/flashcards{?id,id[],question,answer}",
        "hydra:variableRepresentation": "BasicRepresentation",
        "hydra:mapping": [
          {
            "@type": "IriTemplateMapping",
            "variable": "id",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "id[]",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "question",
            "property": "question",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "answer",
            "property": "answer",
            "required": false
          }
        ]
      }
    }
    """

  Scenario: User should be able to get list of flashcards belongs to give subject and lesson
    When user "user1@test.dev" send a "GET" request to "/api/subjects/102b2aca-10e0-4be5-ab4b-5f6198bccfda/lessons/3c1e18f3-d2fb-4559-9cbd-ba2473ddc9d7/flashcards"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Flashcard",
      "@id": "@string@",
      "@type": "hydra:Collection",
      "hydra:member": [
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "beer",
          "answer": "piwo"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "house",
          "answer": "dom"
        }
      ],
      "hydra:totalItems": 2,
      "hydra:search": {
        "@type": "hydra:IriTemplate",
        "hydra:template": "@string@",
        "hydra:variableRepresentation": "BasicRepresentation",
        "hydra:mapping": [
          {
            "@type": "IriTemplateMapping",
            "variable": "id",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "id[]",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "question",
            "property": "question",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "answer",
            "property": "answer",
            "required": false
          }
        ]
      }
    }
    """

  Scenario: Only owner should be able to get flashcard data
    When user "user2@test.dev" send a "GET" request to "/api/flashcards/2a89be22-0639-46c3-b2bc-fa96b8aa6195"
    Then the response status code should be 404
    When user "user1@test.dev" send a "GET" request to "/api/flashcards/2a89be22-0639-46c3-b2bc-fa96b8aa6195"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Flashcard",
      "@id": "\/api\/flashcards\/2a89be22-0639-46c3-b2bc-fa96b8aa6195",
      "@type": "Flashcard",
      "id": "2a89be22-0639-46c3-b2bc-fa96b8aa6195",
      "question": "beer",
      "answer": "piwo"
    }
    """

  Scenario: User should be able create new flashcard when passed data are valid
    When user "user1@test.dev" send a "POST" request to "/api/flashcards" with body:
    """
    {
       "answer": "answer"
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "@context": "@string@",
      "@type": "ConstraintViolationList",
      "hydra:title": "An error occurred",
      "hydra:description": "@string@",
      "violations": @array@
    }
    """
    When user "user1@test.dev" send a "POST" request to "/api/flashcards" with body:
    """
    {
      "answer": "answer",
      "question": "question",
      "lesson": "/api/lessons/3c1e18f3-d2fb-4559-9cbd-ba2473ddc9d7"
    }
    """
    Then the response status code should be 201
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Flashcard",
      "@id": "@string@",
      "@type": "Flashcard",
      "id": "@string@",
      "question": "question",
      "answer": "answer"
    }
    """

  Scenario: Only owner should be able to update flashcard data
    When user "user2@test.dev" send a "PUT" request to "/api/flashcards/2a89be22-0639-46c3-b2bc-fa96b8aa6195" with body:
    """
    {
      "question": "new name"
    }
    """
    Then the response status code should be 404
    When user "user1@test.dev" send a "PUT" request to "/api/flashcards/2a89be22-0639-46c3-b2bc-fa96b8aa6195" with body:
    """
    {
      "question": "new name"
    }
    """
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Flashcard",
      "@id": "@string@",
      "@type": "Flashcard",
      "id": "@string@",
      "question": "new name",
      "answer": "piwo"
    }
    """

  Scenario: Only owner should be able to delete flashcard
    When user "user2@test.dev" send a "DELETE" request to "/api/flashcards/2a89be22-0639-46c3-b2bc-fa96b8aa6195"
    Then the response status code should be 404
    When user "user1@test.dev" send a "DELETE" request to "/api/flashcards/2a89be22-0639-46c3-b2bc-fa96b8aa6195"
    Then the response status code should be 204
