@alice(User)
Feature: Authenticate user

  Scenario: The user should be able to log in if pass correct credentials
    Given clear cache
    When I send a "POST" request to "/api/login" with body:
    """
    {
      "email": "user1@test.dev",
      "password": "testtest"
    }
    """
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "token": "@string@"
    }
    """

  Scenario: The user should not be able to log in if pass incorrect credentials
    When I send a "POST" request to "/api/login" with body:
    """
    {
      "email": "user@test.dev",
      "password": "wrongpassword"
    }
    """
    Then the response status code should be 401