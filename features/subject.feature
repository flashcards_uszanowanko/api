@alice(User) @alice(Subject) @alice(Lesson)
Feature: Mange subjects

  Scenario: User should be able to get list of subjects
    When user "user1@test.dev" send a "GET" request to "/api/subjects"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Subject",
      "@id": "\/api\/subjects",
      "@type": "hydra:Collection",
      "hydra:member": [
        {
          "@id": "@string@",
          "@type": "Subject",
          "id": "@string@",
          "name": "English"
        },
        {
          "@id": "@string@",
          "@type": "Subject",
          "id": "@string@",
          "name": "Polish"
        }
      ],
      "hydra:totalItems": 2,
      "hydra:search": {
        "@type": "hydra:IriTemplate",
        "hydra:template": "\/api\/subjects{?id,id[],name}",
        "hydra:variableRepresentation": "BasicRepresentation",
        "hydra:mapping": [
          {
            "@type": "IriTemplateMapping",
            "variable": "id",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "id[]",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "name",
            "property": "name",
            "required": false
          }
        ]
      }
    }
    """

  Scenario: Only owner should be able to get subject data
    When user "user2@test.dev" send a "GET" request to "/api/subjects/102b2aca-10e0-4be5-ab4b-5f6198bccfda"
    Then the response status code should be 404
    When user "user1@test.dev" send a "GET" request to "/api/subjects/102b2aca-10e0-4be5-ab4b-5f6198bccfda"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Subject",
      "@id": "@string@",
      "@type": "Subject",
      "id": "@string@",
      "name": "English"
    }
    """

  Scenario: User should be able create new subject when passed data are valid
    When user "user1@test.dev" send a "POST" request to "/api/subjects" with body:
    """
    {
      "name": ""
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "@context": "@string@",
      "@type": "ConstraintViolationList",
      "hydra:title": "An error occurred",
      "hydra:description": "@string@",
      "violations": @array@
    }
    """
    When user "user1@test.dev" send a "POST" request to "/api/subjects" with body:
    """
    {
      "name": "test"
    }
    """
    Then the response status code should be 201
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Subject",
      "@id": "@string@",
      "@type": "Subject",
      "id": "@string@",
      "name": "test"
    }
    """

  Scenario: Only owner should be able to update subject data
    When user "user2@test.dev" send a "PUT" request to "/api/subjects/102b2aca-10e0-4be5-ab4b-5f6198bccfda" with body:
    """
    {
      "name": "new name"
    }
    """
    Then the response status code should be 404
    When user "user1@test.dev" send a "PUT" request to "/api/subjects/102b2aca-10e0-4be5-ab4b-5f6198bccfda" with body:
    """
    {
      "name": "new name"
    }
    """
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Subject",
      "@id": "@string@",
      "@type": "Subject",
      "id": "@string@",
      "name": "new name"
    }
    """

  Scenario: Only owner should be able to delete subject
    When user "user2@test.dev" send a "DELETE" request to "/api/subjects/102b2aca-10e0-4be5-ab4b-5f6198bccfda"
    Then the response status code should be 404
    When user "user1@test.dev" send a "DELETE" request to "/api/subjects/102b2aca-10e0-4be5-ab4b-5f6198bccfda"
    Then the response status code should be 204
