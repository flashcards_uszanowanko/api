@alice(User) @alice(Subject) @alice(Lesson) @alice(Answer)
Feature: CQRS

  Scenario: User should be able to get list of answers
    When user "user2@test.dev" send a "GET" request to "/api/flashcard_answers"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/FlashcardAnswer",
      "@id": "\/api\/flashcard_answers",
      "@type": "hydra:Collection",
      "hydra:member": "@array@",
      "hydra:totalItems": 6
    }
    """

  Scenario: User should be able to send simple answer
    When user "user2@test.dev" send a "POST" request to "/api/answer/simple" with body:
    """
    {
      "answer": true,
      "flashcard": "/api/flashcards/0ee7b9ed-595a-44e2-98fa-d4586054ea86"
    }
    """
    Then the response status code should be 201


  Scenario: User should be able to send progress answer
    When user "user2@test.dev" send a "POST" request to "/api/answer/progress" with body:
    """
    {
      "answer": 0,
      "flashcard": "/api/flashcards/0ee7b9ed-595a-44e2-98fa-d4586054ea86"
    }
    """
    Then the response status code should be 201


  Scenario: User should be able to get simple lesson
    When user "user2@test.dev" send a "GET" request to "/api/lesson/6dd5722d-b2fc-4498-9daa-c3577c30b70b/simple/"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/FlashcardSimpleLesson",
      "@id": "\/api\/lesson\/Lektion%25202\/simple\/",
      "@type": "FlashcardSimpleLesson",
      "lesson": "Lektion 2",
      "flashcards": [
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "richtung",
          "answer": "direction"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "betrunken",
          "answer": "drunk"
        },
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "haus",
          "answer": "house"
        }
      ]
    }
    """

  Scenario: User should be able to get progress lesson
    When user "user2@test.dev" send a "GET" request to "/api/lesson/6dd5722d-b2fc-4498-9daa-c3577c30b70b/progress/"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/FlashcardProgressLesson",
      "@id": "\/api\/lesson\/Lektion%25202\/progress\/",
      "@type": "FlashcardProgressLesson",
      "lesson": "Lektion 2",
      "flashcards": [
        {
          "@id": "@string@",
          "@type": "Flashcard",
          "id": "@string@",
          "question": "haus",
          "answer": "house"
        }
      ]
    }
    """

