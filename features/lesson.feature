@alice(User) @alice(Subject) @alice(Lesson)
Feature: Mange lessons

  Scenario: User should be able to get list of lessons
    When user "user1@test.dev" send a "GET" request to "/api/lessons"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Lesson",
      "@id": "\/api\/lessons",
      "@type": "hydra:Collection",
      "hydra:member": [
        {
          "@id": "@string@",
          "@type": "Lesson",
          "id": "@string@",
          "name": "Lekcja 2"
        },
        {
          "@id": "@string@",
          "@type": "Lesson",
          "id": "@string@",
          "name": "Lesson 1"
        },
        {
          "@id": "@string@",
          "@type": "Lesson",
          "id": "@string@",
          "name": "Lekcja 1"
        },
        {
          "@id": "@string@",
          "@type": "Lesson",
          "id": "@string@",
          "name": "Lesson 2"
        }
      ],
      "hydra:totalItems": 4,
      "hydra:search": {
        "@type": "hydra:IriTemplate",
        "hydra:template": "\/api\/lessons{?id,id[],name}",
        "hydra:variableRepresentation": "BasicRepresentation",
        "hydra:mapping": [
          {
            "@type": "IriTemplateMapping",
            "variable": "id",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "id[]",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "name",
            "property": "name",
            "required": false
          }
        ]
      }
    }
    """

  Scenario: User should be able to get list of lessons belongs to give subject
    When user "user1@test.dev" send a "GET" request to "/api/subjects/102b2aca-10e0-4be5-ab4b-5f6198bccfda/lessons"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Lesson",
      "@id": "@string@",
      "@type": "hydra:Collection",
      "hydra:member": [
        {
          "@id": "@string@",
          "@type": "Lesson",
          "id": "@string@",
          "name": "Lesson 1"
        },
        {
          "@id": "@string@",
          "@type": "Lesson",
          "id": "@string@",
          "name": "Lesson 2"
        }
      ],
      "hydra:totalItems": 2,
      "hydra:search": {
        "@type": "hydra:IriTemplate",
        "hydra:template": "@string@",
        "hydra:variableRepresentation": "BasicRepresentation",
        "hydra:mapping": [
          {
            "@type": "IriTemplateMapping",
            "variable": "id",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "id[]",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "name",
            "property": "name",
            "required": false
          }
        ]
      }
    }
    """

  Scenario: Only owner should be able to get lesson data
    When user "user2@test.dev" send a "GET" request to "/api/lessons/3c1e18f3-d2fb-4559-9cbd-ba2473ddc9d7"
    Then the response status code should be 404
    When user "user1@test.dev" send a "GET" request to "/api/lessons/3c1e18f3-d2fb-4559-9cbd-ba2473ddc9d7"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Lesson",
      "@id": "@string@",
      "@type": "Lesson",
      "id": "@string@",
      "name": "Lesson 1"
    }
    """

  Scenario: User should be able create new lesson when passed data are valid
    When user "user1@test.dev" send a "POST" request to "/api/lessons" with body:
    """
    {
      "name": ""
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "@context": "@string@",
      "@type": "ConstraintViolationList",
      "hydra:title": "An error occurred",
      "hydra:description": "@string@",
      "violations": @array@
    }
    """
    When user "user1@test.dev" send a "POST" request to "/api/lessons" with body:
    """
    {
      "name": "test",
      "subject": "/api/subjects/102b2aca-10e0-4be5-ab4b-5f6198bccfda"
    }
    """
    Then the response status code should be 201
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Lesson",
      "@id": "@string@",
      "@type": "Lesson",
      "id": "@string@",
      "name": "test"
    }
    """



  Scenario: Only owner should be able to update lesson data
    When user "user2@test.dev" send a "PUT" request to "/api/lessons/3c1e18f3-d2fb-4559-9cbd-ba2473ddc9d7" with body:
    """
    {
      "name": "new name"
    }
    """
    Then the response status code should be 404
    When user "user1@test.dev" send a "PUT" request to "/api/lessons/3c1e18f3-d2fb-4559-9cbd-ba2473ddc9d7" with body:
    """
    {
      "name": "new name"
    }
    """
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/Lesson",
      "@id": "@string@",
      "@type": "Lesson",
      "id": "@string@",
      "name": "new name"
    }
    """

  Scenario: Only owner should be able to delete lesson
    When user "user2@test.dev" send a "DELETE" request to "/api/lessons/3c1e18f3-d2fb-4559-9cbd-ba2473ddc9d7"
    Then the response status code should be 404
    When user "user1@test.dev" send a "DELETE" request to "/api/lessons/3c1e18f3-d2fb-4559-9cbd-ba2473ddc9d7"
    Then the response status code should be 204
