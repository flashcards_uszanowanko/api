@alice(User)
Feature: Mange user accounts

  Scenario: Everybody should be able to create new account
    When I send a "POST" request to "/api/users" with body:
    """
    {
      "name": "Test",
      "email": "test@test.dev",
      "roles": [
        "ROLE_USER"
      ],
      "plainPassword": "password"
    }
    """
    Then the response status code should be 201
    And the response should contain json:
    """
    {
      "@context": "@string@",
      "@id": "@string@",
      "@type": "User",
      "id": "@string@",
      "name": "Test",
      "email": "test@test.dev",
      "roles": [
        "ROLE_USER"
      ]
    }
    """

  Scenario: To create account passed data should by valid
    When I send a "POST" request to "/api/users" with body:
    """
    {
      "name": "",
      "email": "user@test.dev",
      "roles": [
        "ROLE_USERs"
      ],
      "plainPassword": "wrong"
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "@context": "@string@",
      "@type": "ConstraintViolationList",
      "hydra:title": "An error occurred",
      "hydra:description": "@string@",
      "violations": @array@
    }
    """

  Scenario: Only user with role ROLE_ADMIN should be able to get full user list
    When user "admin@test.dev" send a "GET" request to "/api/users"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/User",
      "@id": "\/api\/users",
      "@type": "hydra:Collection",
      "hydra:member": [
        {
          "@id": "@string@",
          "@type": "User",
          "id": "@string@",
          "name": "Admin",
          "email": "admin@test.dev"
        },
        {
          "@id": "@string@",
          "@type": "User",
          "id": "@string@",
          "name": "User",
          "email": "user1@test.dev"
        },
        {
          "@id": "@string@",
          "@type": "User",
          "id": "@string@",
          "name": "User",
          "email": "user2@test.dev"
        }
      ],
      "hydra:totalItems": 3,
      "hydra:search": {
        "@type": "hydra:IriTemplate",
        "hydra:template": "/api/users{?id,id[],name,email}",
        "hydra:variableRepresentation": "BasicRepresentation",
        "hydra:mapping": [
          {
            "@type": "IriTemplateMapping",
            "variable": "id",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "id[]",
            "property": "id",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "name",
            "property": "name",
            "required": false
          },
          {
            "@type": "IriTemplateMapping",
            "variable": "email",
            "property": "email",
            "required": false
          }
        ]
      }
    }
    """
    When user "user1@test.dev" send a "GET" request to "/api/users"
    Then the response status code should be 403

  Scenario: Only owner or user with role ROLE_ADMIN can get user data
    When user "user1@test.dev" send a "GET" request to "/api/users/acc64936-ecd6-43d3-9c83-512595ad838b"
    Then the response status code should be 200
    When user "user2@test.dev" send a "GET" request to "/api/users/acc64936-ecd6-43d3-9c83-512595ad838b"
    Then the response status code should be 403
    When user "admin@test.dev" send a "GET" request to "/api/users/acc64936-ecd6-43d3-9c83-512595ad838b"
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "@string@",
      "@id": "@string@",
      "@type": "User",
      "id": "@string@",
      "name": "User",
      "email": "user1@test.dev",
      "roles": [
        "ROLE_USER"
      ]
    }
    """

  Scenario: Only owner or user with role ROLE_ADMIN can change user data if passed data are valid
    When user "user1@test.dev" send a "PUT" request to "/api/users/acc64936-ecd6-43d3-9c83-512595ad838b" with body:
    """
    {
      "name": "New Name"
    }
    """
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "@string@",
      "@id": "@string@",
      "@type": "User",
      "id": "@string@",
      "name": "New Name",
      "email": "user1@test.dev",
      "roles": [
        "ROLE_USER"
      ]
    }
    """
    When user "user2@test.dev" send a "PUT" request to "/api/users/acc64936-ecd6-43d3-9c83-512595ad838b" with body:
    """
    {
      "name": "New Name"
    }
    """
    Then the response status code should be 403
    When user "admin@test.dev" send a "PUT" request to "/api/users/acc64936-ecd6-43d3-9c83-512595ad838b" with body:
    """
    {
      "name": ""
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/ConstraintViolationList",
      "@type": "ConstraintViolationList",
      "hydra:title": "An error occurred",
      "hydra:description": "name: This value should not be blank.",
      "violations": [
        {
          "propertyPath": "name",
          "message": "This value should not be blank."
        }
      ]
    }
    """

  Scenario: Only owner or user with role ROLE_ADMIN can change user password if passed data are valid
    When user "user1@test.dev" send a "PUT" request to "/api/users/acc64936-ecd6-43d3-9c83-512595ad838b/change-password" with body:
    """
    {
      "plainPassword": "NewPassword"
    }
    """
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "@context": "@string@",
      "@id": "@string@",
      "@type": "User",
      "id": "@string@",
      "name": "User",
      "email": "user1@test.dev",
      "roles": [
        "ROLE_USER"
      ]
    }
    """
    When user "user2@test.dev" send a "PUT" request to "/api/users/acc64936-ecd6-43d3-9c83-512595ad838b/change-password" with body:
    """
    {
      "plainPassword": "NewPassword"
    }
    """
    Then the response status code should be 403
    When user "admin@test.dev" send a "PUT" request to "/api/users/acc64936-ecd6-43d3-9c83-512595ad838b/change-password" with body:
    """
     {
      "plainPassword": "wrong"
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "@context": "\/api\/contexts\/ConstraintViolationList",
      "@type": "ConstraintViolationList",
      "hydra:title": "An error occurred",
      "hydra:description": "plainPassword: This value is too short. It should have 8 characters or more.",
      "violations": [
        {
          "propertyPath": "plainPassword",
          "message": "This value is too short. It should have 8 characters or more."
        }
      ]
    }
    """