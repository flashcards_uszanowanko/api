
# Setup project
### Run project

```sh
docker-compose build
docker-compose up -d
```

### Enter to docker container
```sh
docker-compose exec app sh
```

### Setup/Update database
```sh
bin/console doctrine:schema:update -f
```


### Project

Project is available on

```
https://docker-ip/  (https://192.168.99.100/ https://localhost/)
```

### Api documentation

Api documentation is available on

```
https://docker-ip/api  (https://192.168.99.100/api https://localhost/api)
```

## Troubleshooting

### sometimes is needed to rebuild docker image for example to generate new JWT ssh keys
```sh
docker-compose build --no-cache --force-rm
```